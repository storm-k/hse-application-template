Copyright (c) 2017-2021, HAMPEL SOFTWARE ENGINEERING.

The Modules and Module Templates in this repository are provided by 
HAMPEL SOFTWARE ENGINEERING (www.hampel-soft.com) and are created for 
and based on the Delacor Queued Message Handler.

The Delacor Queued Message Handler (DQMH) reference design is provided by 
Delacor, an NI Alliance Partner. The DQMH is available for download from 
the LabVIEW Tools Network. You can find more information on the Delacor 
Queued Message Handler (DQMH) at http://delacor.com/products/dqmh/.

Redistribution of this library in source form to third parties is prohibited. 
Use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

* Any product using this library will include in its About Box or other 
  applicable written documentation the notice that the given product 
  "Includes HSE Windows Application Template, © (2021) HAMPEL SOFTWARE 
  ENGINEERING. All rights reserved."

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.